Project migrated to https://gitlab.com/caosdb

# Welcome
CaosDB is a software toolbox for data management. Its focus lies on managing 
data in dynamic environments of research and development where changes are too 
frequent or projects are too small to bear the great cost of implementing a 
traditional specialized but inflexible data management system. The research 
data management system (RDMS) CaosDB uses a semantic data model that allows to 
grow and change in those dynamic environments!

This is the CaosDB root repository. It is combines all publicly available
official repositories of caosdb and serves as an entry point for users and
developers.

- [caosdb-pylib](https://gitlab.gwdg.de/bmp-caosdb/caosdb-pylib?nav_source=navbar) 
  contains the python client. Typically you want to start here. You
  can for example access the demo server with the client.
- [caosdb-server](https://gitlab.gwdg.de/bmp-caosdb/caosdb-server?nav_source=navbar) 
  contains the java sources of the server software and should be 
  installed on ... the server. This is was you need for your own CaosDB 
  instance.
- [caosdb-mysqlbackend](https://gitlab.gwdg.de/bmp-caosdb/caosdb-mysqlbackend?nav_source=navbar) 
  contains sources for setting up and interacting with the 
  MySQL/MariaDB backend. You need it for running the server.
- [caosdb-webui](https://gitlab.gwdg.de/bmp-caosdb/caosdb-webui?nav_source=navbar) 
  contains the web client (is typically installed with 
  caosdb-server and is a submodule thereof).
- [caosdb-pyinttest](https://gitlab.gwdg.de/bmp-caosdb/caosdb-pyinttest?nav_source=navbar)
  contains integration test for the system.

# License

Copyright (C) 2018 Research Group Biomedical Physics, Max Planck Institute for
Dynamics and Self-Organization, Göttingen.

Generally, the CaosDB project and all files in the official repositories are
licensed under a [GNU Affero General Public License](LICENSE.md) (version 3 or
later).  However, always check the subrepositories' `LICENSE.md` files for the
license which applies in the particular case.

# Maintainers

* Timm Fitschen (Lead)
* Daniel Hornung
* Alexander Schlemmer
* Henrik tom Wörden


# Further reading
* [Contributing](CONTRIBUTING.md)
* [License](LICENSE.md)
* [Code of Conduct](CODE_OF_CONDUCT.md)
* [Humans](HUMANS.md)

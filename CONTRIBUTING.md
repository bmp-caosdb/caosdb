Thank you very much to all contributers—[past, present](HUMANS.md), and prospective ones.

# Code of Conduct

By participating, you are expected to uphold our [Code of Conduct](CODE_OF_CONDUCT.md).

# How to Contribute

* You found a bug, have a question, or want to request a featuer? Please file a Gitlab issue.
* You want to contribute code or write documentation? There is no standardized way to contribute code yet. Please contact us at **info (AT) caosdb.de** if there has not been any prior contact.

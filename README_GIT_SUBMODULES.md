# Setup

After cloning this repository, if you want to work in or with all submodules or
with a single submodule `S`, you need to:

* Run `git submodule update --init --recursive [-- <path of S>]`.

# View status

Status of all subrepos:

* Run `git submodule status --recursive`. The output looks like this:

         -f1906060a318ef63a3aa822395308beeb2c45f9f caosdb-archive
         U9729ab59a755b74a9cbe64dcec2d4d7275cce83a caosdb-dev-tools (heads/master)
         +6733b7dc2361eb8c306d3f7184cf79c61335c951 caosdb-mysqlbackend (heads/master)
          c430dcac6ef0f23e261b65084adce6daafc502f5 caosdb-pyinttest (heads/master)
          94868a65c4b10324e4a37513d322a0225ce16cb1 caosdb-pylib (heads/master)
          8cee5b28df7d45270d5f4b55db2864eb3efdeaa5 caosdb-server (heads/master)
          ded8860d6cd6bebae1c0159fe2600e0d1c3a94b3 caosdb-server/caosdb-webui (heads/master)
          ded8860d6cd6bebae1c0159fe2600e0d1c3a94b3 caosdb-webui (heads/master)

    From the man-page (man git-submodules):

    > Show the status of the submodules. This will print the SHA-1 of the currently checked out commit
    > for each submodule, along with the submodule path and the output of git describe for the SHA-1.
    > Each SHA-1 will possibly be prefixed with - if the submodule is not initialized, + if the
    > currently checked out submodule commit does not match the SHA-1 found in the index of the
    > containing repository and U if the submodule has merge conflicts.

* Run `git submodule foreach 'git status'`. This will run `git status` in every
  submodule. When you run this command directly after a fresh clone, you will
  notice that the submodules have *detached HEADS*, i.e. no branch is checked
  out. So **be careful if you work in the submodules** and **checkout a branch
  first** before trying to make changes.

# Use Cases

## Server-only

* Assume you want to work exclusively with the caosdb-server repository.
* After a fresh clone, run `git submodule updaste --init -- caosdb-server` in
  this repository.
* Now go to the caosdb-server repo and **check out a branch** with e.g. `git
  checkout dev`.  Make your changes, commit everything there.
* Then leave the repository in the state that you want to have in the super
  repository (clean working dir, the branch is checked out).
* In the super repository (`caosdb`), run `git status` to see that your
  submodule has changed. Then `add` and `commit` if everything is correct.

## Server plus WebUI

* Working on the server and the WebUI needs a little more setup. Basically do
  everything you already did in the *Server-only* scenario. It is recommended to
  work in `./caosdb-server/caosdb-webui` instead of working in the top-level
  `./caosdb-webui` repository directly.
* Additonally, you need to `update --init` the `caosdb-webui` submodule of the
  `caosdb-server` too.
* If you commit changes in the webui, you also need to commit the new status of
  the submodule in the `caosdb-server` repository, and then in the root
  repository.

# Rules

Some rules for working with submodules in the CaosDB Project:

1. The master branch is sacred. Thou shalt not defile the master branch
   with non-master commits in the submodules.
2. The dev branch is semi-sacred. Thou shalt not defile the dev branch with
   commits from branches other than master or dev in the submodules.

  *Explained:* When commiting in the master (or dev) branch, make sure that all
  your submodules also have the master (dev or master) branch checked out. At
  least, they must have a commit which belongs to the master(dev or master)
  branch checked out. This assures that only master code comes with the master
  branch and only dev or master code comes with the dev branch.

  Feature branches, on the other side, may reference any commit they like.

3. Thou shalt not merge in the root repository. Thou shalt merge only in the
   submodules.

  *Explained:* If submodules merge the feature branches into their dev branch or
  the dev branch into the master branch, it is not necessary and not helpful to
  merge the branches of the root repository. Just commit the new state of the
  submodules.

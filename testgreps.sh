#!/bin/bash
# Grep for keywords in order to check for code fragments or comments.
# A. Schlemmer, 10/2018

# using ripgrep (rg)

GRCMD="rg --iglob !testgreps.sh "
# GRCMD="grep -R"

$GRCMD fitsch

for kword in dumiatis smaxx ;
do
    $GRCMD $kword
done

